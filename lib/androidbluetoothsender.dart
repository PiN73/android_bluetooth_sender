import 'dart:async';

import 'package:flutter/services.dart';

class Androidbluetoothsender {
  static const MethodChannel _channel =
      MethodChannel('p/androidbluetoothsender');

  static Future<void> connect(String mac) async {
    await _channel.invokeMethod<void>('connect', mac);
  }

  static Future<void> send(String text) async {
    await _channel.invokeMethod<void>('send', text);
  }
}
