# androidbluetoothsender

Flutter Android plugin for sending data through bluetooth
to devices like Arduino bluetooth module


## Make sure that

* Bluetooth is enabled

* Device with given MAC address is paired


## Usage

* Connect:
`await Androidbluetoothsender.connect('mac address');`

* Send data:
`await Androidbluetoothsender.send('asd');`

If calling from main() Flutter function, call
`WidgetsFlutterBinding.ensureInitialized();`
first
