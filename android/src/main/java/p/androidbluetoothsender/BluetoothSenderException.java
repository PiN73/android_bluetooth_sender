package p.androidbluetoothsender;

public class BluetoothSenderException extends Exception {

    public BluetoothSenderException() {
    }

    public BluetoothSenderException(String message) {
        super(message);
    }

    public BluetoothSenderException(Throwable cause) {
        super(cause);
    }

    public BluetoothSenderException(String message, Throwable cause) {
        super(message, cause);
    }
}
