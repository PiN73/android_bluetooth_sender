package p.androidbluetoothsender;

import android.util.Log;
import androidx.annotation.NonNull;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

class AndroidbluetoothsenderMethodCallHandler implements MethodChannel.MethodCallHandler {
    private BluetoothSender bluetoothSender;

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
        if (call.method.equals("connect")) {
            try {
                bluetoothSender = new BluetoothSender((String) call.arguments);
                result.success(null);
            } catch (Exception e) {
                result.error(call.method, Log.getStackTraceString(e), null);
            }
        } else if (call.method.equals("send")) {
            try {
                bluetoothSender.send((String) call.arguments);
                result.success(null);
            } catch (Exception e) {
                result.error(call.method, Log.getStackTraceString(e), null);
            }
        } else {
            result.notImplemented();
        }
    }
}
