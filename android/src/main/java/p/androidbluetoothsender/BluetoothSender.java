package p.androidbluetoothsender;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

class BluetoothSender {

    private String MAC;
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private OutputStream outStream;

    BluetoothSender(String MAC) throws BluetoothSenderException {
        this.MAC = MAC;
        choose_device();
        create_socket();
        connect_socket();
        // socket.close() ?
    }

    void send(String text) throws BluetoothSenderException {
        try {
            outStream.write(text.getBytes());
        } catch (IOException e) {
            throw new BluetoothSenderException("Error sending (IOException)", e);
        } catch (Exception e) {
            throw new BluetoothSenderException("Error sending", e);
        }
    }

    private void choose_device() throws BluetoothSenderException {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter == null) {
            throw new BluetoothSenderException("can not access bluetooth adapter");
        }
        if (!adapter.isEnabled()) {
            throw new BluetoothSenderException("bluetooth is disabled");
        }

        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();

        for (BluetoothDevice d : pairedDevices) {
            // String name = d.getName();
            String mac = d.getAddress();
            if (mac.equals(MAC))
                device = d;
        }

        if (device == null) {
            throw new BluetoothSenderException("no paired device found with MAC = " + MAC);
        }
    }

    private void create_socket() throws BluetoothSenderException {
        /*final UUID MY_UUID_SECURE =
                UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
        final UUID MY_UUID_INSECURE =
                UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");*/
        final UUID MY_ID_OTHER =
                UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

        try {
            socket = device.createRfcommSocketToServiceRecord(MY_ID_OTHER);
        } catch (IOException e) {
            throw new BluetoothSenderException("unable to create socket", e);
        }
    }

    private void connect_socket() throws BluetoothSenderException {
        final int attempts = 10;
        for (int i = 0; i < attempts; ++i) {
            try {
                socket.connect();
                // logger.log(prints("socket successfully connected (attempt %d)", i + 1));
                break;
            } catch (IOException e) {
                // logger.log(prints("unable to connect socket (attempt %d / %d)", i + 1, attempts));
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e1) {/**/}
            }
        }
        if (!socket.isConnected()) {
            throw new BluetoothSenderException("unable to connect socket");
        }

        try {
            outStream = socket.getOutputStream();
        } catch (IOException e) {
            throw new BluetoothSenderException("unable to create output stream", e);
        }
    }

    private String prints(String s, Object... args) {
        return String.format(Locale.getDefault(), s, args);
    }
}