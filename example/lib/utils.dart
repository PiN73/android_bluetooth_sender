import 'dart:io';

extension FileExtension on File {
  Future<File> appendAsString(String contents) =>
      writeAsString(contents, mode: FileMode.writeOnlyAppend);
}

extension DirectoryExtension on Directory {
  Directory dir(String name) => Directory('$path/$name');
  File file(String name) => File('$path/$name');
}
