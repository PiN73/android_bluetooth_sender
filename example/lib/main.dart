import 'dart:async';
import 'dart:io';
import 'package:androidbluetoothsender_example/logger.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:androidbluetoothsender/androidbluetoothsender.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final logger = await localFileLogger();
  FlutterError.onError = logger.logError;
  await runZonedGuarded<Future<void>>(() async {
    runApp(MyApp());
  }, logger.log);
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

Future<String> readMac() async {
  final dir = await getExternalStorageDirectory();
  final file = File('${dir!.path}/MAC.txt');
  return file.readAsString();
}

class _MyAppState extends State<MyApp> {
  Future<void> connect() async {
    final mac = await readMac();
    await Androidbluetoothsender.connect(mac);
  }

  @override
  void initState() {
    connect();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TestPage(),
    );
  }
}

class TestPage extends StatefulWidget {
  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {
  late TextEditingController controller;

  @override
  void initState() {
    controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: TextField(
            controller: controller,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              hintText: 'Input string',
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.send),
        onPressed: () async {
          final text = controller.text;
          if (text.isNotEmpty) {
            await Androidbluetoothsender.send(text);
            controller.clear();
          }
        },
      ),
    );
  }
}
