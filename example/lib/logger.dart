import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:androidbluetoothsender_example/utils.dart';

/// Example:
/// void main() async {
///   WidgetsFlutterBinding.ensureInitialized();
///   final logger = await localFileLogger();
///   FlutterError.onError = logger.logError;
///   await runZoned<Future<void>>(() async {
///     runApp(MyApp());
///   }, onError: logger.log);
/// }
///
/// https://stackoverflow.com/a/56453756/6131611
class Logger {
  final Future<void> Function(String) write;

  Logger({required this.write});

  Future<void> logError(FlutterErrorDetails details) async {
    // FlutterError.dumpErrorToConsole(details);
    await _write(details.exceptionAsString(), details.stack.toString());
  }

  Future<void> log(Object data, StackTrace stackTrace) async {
    // print(data);
    // print(stackTrace);
    await _write(data.toString(), stackTrace.toString());
  }

  Future<void> _write(String a, String b) async {
    final date = DateTime.now().toString();
    final s = [date, a, b, ''].join('\n');
    await write(s);
  }
}

Future<Logger> localFileLogger() async {
  final rootDir = await getExternalStorageDirectory();
  final logsDir = await rootDir!.dir('logs').create();
  final now = DateTime.now();
  final file = logsDir.file('$now.log');
  return Logger(write: file.appendAsString);
}
